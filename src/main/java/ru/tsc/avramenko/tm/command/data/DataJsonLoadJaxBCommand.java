package ru.tsc.avramenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.Domain;
import ru.tsc.avramenko.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load json data from file (JaxB).";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty(SYSTEM_JSON_PROPERTY_NAME, SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(JAXB_JSON_PROPERTY_NAME, JAXB_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}