package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class UserChangeRoleCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-change-role";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change the user role.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findById(id);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER NEW ROLE: ");
        System.out.println(Arrays.toString(Role.values()));
        @Nullable final String roleValue = TerminalUtil.nextLine();
        @NotNull final Role role = Role.valueOf(roleValue);
        serviceLocator.getUserService().setRole(id, role);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}